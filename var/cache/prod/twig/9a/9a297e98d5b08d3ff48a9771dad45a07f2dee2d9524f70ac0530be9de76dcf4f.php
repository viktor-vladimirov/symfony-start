<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @App/product/show.html.twig */
class __TwigTemplate_627927c078f8a84443128f5644e600bb53e27340a963c28c85e5b3c5550d7233 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'title' => [$this, 'block_title'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@App/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("@App/layout.html.twig", "@App/product/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        // line 4
        echo "    <h2>Product page ID ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? null), "id", []), "html", null, true);
        echo "</h2>
    Title: ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? null), "title", []), "html", null, true);
        echo "<br>
    Price: ";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? null), "price", []), "html", null, true);
        echo "\$<br>
    Category: ";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["product"] ?? null), "category", []), "name", []), "html", null, true);
        echo "<br>
    Description: ";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? null), "description", []), "html", null, true);
        echo "<br>

";
    }

    // line 12
    public function block_title($context, array $blocks = [])
    {
        echo "Product Page";
    }

    public function getTemplateName()
    {
        return "@App/product/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 12,  60 => 8,  56 => 7,  52 => 6,  48 => 5,  43 => 4,  40 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@App/product/show.html.twig", "/home/muser/lesson-symfony/src/AppBundle/Resources/views/product/show.html.twig");
    }
}
