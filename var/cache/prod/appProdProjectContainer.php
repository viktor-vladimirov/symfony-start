<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerKqlkqb8\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerKqlkqb8/appProdProjectContainer.php') {
    touch(__DIR__.'/ContainerKqlkqb8.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\ContainerKqlkqb8\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \ContainerKqlkqb8\appProdProjectContainer([
    'container.build_hash' => 'Kqlkqb8',
    'container.build_id' => '21624982',
    'container.build_time' => 1598346688,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerKqlkqb8');
