<?php


namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CategoryController extends Controller
{
    /**
     * @Route("/categories", name="categoryList")
     * @Template()
     */
    public function indexAction()
    {
        $category = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findBy([],['name' => 'DESC']);

         return ['category' => $category];
    }

}