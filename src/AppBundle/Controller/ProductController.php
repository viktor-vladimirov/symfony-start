<?php


namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ProductController extends Controller
{
    /**
     * @Route("/products", name="productList")
     * @Template()
     */
    public function indexAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('AppBundle:Product')
            ->findActive();

         return ['products' => $products];
    }

    /**
     * @Route("/products/{id}", name="productItem", requirements={"id": "[0-9]+"})
     * @Template()
     * @param Product $product
     * @return Product[]
     */
    public function showAction(Product $product)
    {
        return ['product' => $product];
    }

    /**
     * @Route("/category/{id}", name="list_by_category")
     * @Template()
     * @param Category $category
     * @return array
     */
    public function listByCategoryAction(Category $category)
    {
        $products = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Product')
            ->findByCategory($category);
        return ['products' => $products];
    }
}